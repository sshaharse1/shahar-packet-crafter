#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <strings.h>

#include <linux/ip.h>
#include <linux/udp.h>
#include <cstring>
#include "packet/packet.h"


int handle_port_knock() {
    int sock_raw = socket(PF_INET, SOCK_RAW, 17);
    int msglen;
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(1234);
    unsigned char buffer[8096];

    memset(buffer, 0, 8096);
    if(bind(sock_raw, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        perror("Couldnt bind :(");
        exit(EXIT_FAILURE);
    }

    msglen = recv(sock_raw, &buffer, 8096, 0);
    UDPPacket packet(buffer, msglen);
    std::cout << msglen << std::endl;
    std::cout << packet.get_udp_data() << std::endl;
    packet.print_buffer();
    std::cout << "------" << std::endl;
    std::cout << packet.get_source_port() << std::endl;
    std::cout << packet.get_destination_port() << std::endl;
    std::cout << packet.get_destination_ip() << std::endl;
    std::cout << packet.get_source_ip() << std::endl;
}



int main() {
    std::cout << "Hello, World!" << std::endl;
    handle_port_knock();
    return 0;
}
