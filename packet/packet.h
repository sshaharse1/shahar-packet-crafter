
//
// Created by shahar on 8/25/19.
//

#ifndef SNORLAX_SNIFF_H
#define SNORLAX_SNIFF_H

struct Buffer {
    unsigned char* data;
    int length;
};

class UDPPacket {

public:
    UDPPacket(unsigned char *buff, int length);
    unsigned char *get_udp_data();
    void print_buffer();
    int get_source_port();
    int get_destination_port();
    std::string get_destination_ip();
    std::string get_source_ip();

private:
    struct Buffer data;
    int hex_char_array_to_int(unsigned char* array, int length);

};


#endif //SNORLAX_SNIFF_H
