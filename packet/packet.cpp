//
// Created by shahar on 8/25/19.
//
#include <iostream>
#include <stdio.h>
#include <iosfwd>
#include <sstream>
#include "packet.h"

#define DATA_SPOT 28
#define SOURCE_PORT_SPOT 20
#define DESTINATION_PORT_SPOT 22
#define DESTINATION_IP_SPOT 16
#define SOURCE_IP_SPOT 12
#define IP_LENGTH 4


UDPPacket::UDPPacket(unsigned char* buff, int length) {
    data.data = buff;
    data.length = length;
}

unsigned char *UDPPacket::get_udp_data() {
    return &data.data[DATA_SPOT];
}

int UDPPacket::get_source_port() {
    return hex_char_array_to_int(&data.data[SOURCE_PORT_SPOT], 2);
}

int UDPPacket::get_destination_port() {
    return hex_char_array_to_int(&data.data[DESTINATION_PORT_SPOT], 2);
}

std::string UDPPacket::get_source_ip() {
    std::stringstream ip;
    for(int i=0; i<IP_LENGTH; i++)
    {
        ip << hex_char_array_to_int(&data.data[DESTINATION_IP_SPOT + i], 1);
        if(i != IP_LENGTH - 1)
        {
            ip << ".";
        }
    }
    return ip.str();
}

std::string UDPPacket::get_destination_ip() {
    std::stringstream ip;
    for(int i=0; i<IP_LENGTH; i++)
    {
        ip << hex_char_array_to_int(&data.data[SOURCE_IP_SPOT + i], 1);
        if(i != IP_LENGTH - 1)
        {
            ip << ".";
        }
    }
    return ip.str();
}

void UDPPacket::print_buffer() {
    for(int i=0; i<data.length; i++)
    {
        printf("%02X", data.data[i]);
    }
}

int UDPPacket::hex_char_array_to_int(unsigned char* array, int length) {
    int result;
    std::stringstream hex;
    for(int i=0; i<length; i++)
    {
        hex << std::hex << (int)array[i];
    }
    hex >> std::hex >> result;
    return result;
}